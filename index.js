//npm install --save socket.io
//npm i --save lodash

var io = require('socket.io'),
    http = require('http'),
    server = http.createServer(),
    io = io.listen(server);

var _ = require('lodash');


var count = 0;
var users = [];
var superusers = [];

io.use(function(socket, next){
    // return the result of next() to accept the connection.
    var id = socket.id;
    var usuario = socket.handshake.query.user;
    console.log('Usuario Conectado: ' + id + ': ' + socket.handshake.query.user);
    socket.join(id);

    if ( usuario.startsWith("#") ) {
        superusers.push({'id': id , 'socket': socket});
    } else {
        addUser(id, '@'+usuario);
    }    
    wakeSupers(id);

    return next();
    // call next() with an Error if you need to reject the connection.
    //next(new Error('Authentication error'));
});

io.on('connection', function(socket){
    var id = socket.id;

    socket.on('disconnect', function(){
        console.log('Usuario Desconectado => ' + id);
        removeUser(id);
    });

    socket.on('message', function(msg){
        if ( msg.userName.startsWith("#") ) {
            superusers.push({'id': id , 'socket': socket});
            wakeSupers(id);
            if ( msg.message.startsWith('@') ){
                io.to(getIdUserNameFromMessage(msg.message)).emit('message',msg);
            }
        } else {
            console.log('Chegou mensagem > ' + id + ' - ' + msg.userName + ":" + msg.message);
            addUser(id, '@'+msg.userName);
            io.to(id).emit('message',msg);
        }
    });
})

server.listen(3000, function(){
    console.log('Servidor iniciado porta 3000');
})

function wakeSupers(id){
    console.log(superusers);
    _.forEach(superusers, function(value, key) {
        console.log('Acordando super ususario - ' + value);
        console.log(value.id + "  <---->  " + key);
        var sock = value.socket;
        _.forEach(users, function(value, key) {
            console.log(value.id);
            sock.join(value.id);
        });
    });
}

function addUser(id, nome){
    if ( _.findIndex(users, { 'id': id }) == -1 ){
        if ( nome == '@') nome = '@' + id;
        users.push({'id': id , 'nome': nome});
    }
    console.log(users);
}

function removeUser(id){
    users = _.dropWhile(users, {'id': id });
    console.log(users);
}

function getKey(userName) {
    return _.find(users, { 'nome': userName }).id;
}

function getIdUserNameFromMessage(message){
    console.log(message.substr(0,message.indexOf(' ')));
    console.log(getKey(message.substr(0,message.indexOf(' '))));
    return getKey(message.substr(0,message.indexOf(' ')));
}

